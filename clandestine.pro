TEMPLATE = subdirs

CONFIG = warn_off c++11

SUBDIRS = \
base \
coremods \
modules

coremods.subdir = coremods/
modules.subdir = modules/
base.subdir = base/
