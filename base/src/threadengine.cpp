/*
 * clandestine -- Internet Relay Chat Daemon
 *
 *   Copyright (C) 2014 Attila Molnar <attilamolnar@hush.com>
 *   Copyright (C) 2012 Robby <robby@chatbelgie.be>
 *   Copyright (C) 2009 Uli Schlachter <psychon@clandestine.org>
 *   Copyright (C) 2009 Daniel De Graaf <danieldg@clandestine.org>
 *   Copyright (C) 2008-2009 Craig Edwards <brain@clandestine.org>
 *
 * This file is part of clandestine.  clandestine is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, version 2.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "clandestine.h"

void Thread::SetExitFlag()
{
	ExitFlag = true;
}

void Thread::join()
{
	ServerInstance->Threads.Stop(this);
}
