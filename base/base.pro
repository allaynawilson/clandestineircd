CONFIG = warn_off c++11 shared

SOURCES += \
src/bancache.cpp \
src/base.cpp \
src/channels.cpp \
src/cidr.cpp \
src/clientprotocol.cpp \
src/command_parse.cpp \
src/commands.cpp \
src/configparser.cpp \
src/configreader.cpp \
src/cull_list.cpp \
src/dynamic.cpp \
src/filelogger.cpp \
src/fileutils.cpp \
src/hashcomp.cpp \
src/helperfuncs.cpp \
src/inspsocket.cpp \
src/inspstring.cpp \
src/listensocket.cpp \
src/listmode.cpp \
src/logger.cpp \
src/mode.cpp \
src/modulemanager.cpp \
src/modules.cpp \
src/serializable.cpp \
src/server.cpp \
src/snomasks.cpp \
src/socket.cpp \
src/socketengine.cpp \
src/threadengine.cpp \
src/timer.cpp \
src/usermanager.cpp \
src/users.cpp \
src/wildcard.cpp \
src/xline.cpp \
src/clandestine.cpp \
src/threadengines/threadengine_pthread.cpp \
src/socketengines/socketengine_epoll.cpp

INCLUDEPATH += ../
INCLUDEPATH += include/
INCLUDEPATH += include/threadengines/

OBJECTS_DIR = ../out/obj/base

TARGET = ../out/sbin/clandestine
