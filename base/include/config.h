/*
 * clandestine -- Internet Relay Chat Daemon
 *
 *   Copyright (C) 2014, 2016, 2018-2021 Sadie Powell <sadie@witchery.services>
 *
 * This file is part of clandestine.  clandestine is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, version 2.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#pragma once

/** The branch version that is shown to unprivileged users. */
#define CLANDESTINE_BRANCH "clandestine-mainline"

/** The full version that is shown to privileged users. */
#define CLANDESTINE_VERSION "clandestine-1.0.0"

/** Determines whether this version of clandestine is older than the requested version. */
#define CLANDESTINE_VERSION_BEFORE(MAJOR, MINOR) (((3 << 8) | 11) < ((MAJOR << 8) | (MINOR)))

/** Determines whether this version of clandestine is equal to or newer than the requested version. */
#define CLANDESTINE_VERSION_SINCE(MAJOR, MINOR) (((3 << 8) | 11) >= ((MAJOR << 8) | (MINOR)))

/** The default location that config files are stored in. */
#define CLANDESTINE_CONFIG_PATH "/usr/local/clandestine/etc"

/** The default location that data files are stored in. */
#define CLANDESTINE_DATA_PATH "/usr/local/clandestine/var"

/** The default location that log files are stored in. */
#define CLANDESTINE_LOG_PATH "/usr/local/clandestine/var/log"

/** The default location that module files are stored in. */
#define CLANDESTINE_MODULE_PATH "/usr/local/clandestine/lib/clandestine/modules"

/** The default location that runtime files are stored in. */
#define CLANDESTINE_RUNTIME_PATH "/usr/local/clandestine/var/run"

/** The URL of the clandestine docs site. */
#define CLANDESTINE_DOCS "https://docs.clandestine.network/ver/"
