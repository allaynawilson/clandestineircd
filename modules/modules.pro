TEMPLATE = subdirs

# m_hash_gnutls \
# m_ssl_gnutls \
# m_ldap \
# m_ldapauth \
# m_ldapoper \
# m_mysql \
# m_pgsql \
# m_regex_glob \
# m_regex_pcre \
# m_regex_pcre2 \
# m_regex_posix \
# m_regex_re2 \
# m_regex_stdlib \
# m_regex_tre \
# m_ssl_mbedtls \
# m_sqlite3 \

SUBDIRS = \
m_spanningtree \
m_zombie \
m_abbreviation \
m_alias \
m_allowinvite \
m_alltime \
m_anticaps \
m_antirandom \
m_argon2 \
m_asciiswitch \
m_asn \
m_auditorium \
m_autoaway \
m_autodrop \
m_autokick \
m_autoop \
m_banexception \
m_bannegate \
m_banredirect \
m_bcrypt \
m_blockamsg \
m_blockcaps \
m_blockcolor \
m_blockhighlight \
m_blockinvite \
m_botmode \
m_callerid \
m_cap \
m_cban \
m_censor \
m_cgiirc \
m_chancreate \
m_chanfilter \
m_changecap \
m_chanhistory \
m_chanlog \
m_channames \
m_channelban \
m_check \
m_checkbans \
m_chghost \
m_chgident \
m_chgname \
m_classban \
m_clearchan \
m_clientcheck \
m_cloaking \
m_clones \
m_close \
m_codepage \
m_commonchans \
m_complete \
m_conn_accounts \
m_conn_banner \
m_connectban \
m_connflood \
m_conn_join \
m_conn_join_geoip \
m_conn_join_ident \
m_conn_matchident \
m_conn_require \
m_conn_strictsasl \
m_conn_umodes \
m_conn_vhost \
m_conn_waitpong \
m_custompenalty \
m_customprefix \
m_customtags \
m_customtitle \
m_cycle \
m_dccallow \
m_dccblock \
m_deaf \
m_defaulttopic \
m_delayjoin \
m_delaymsg \
m_denychans \
m_disable \
m_discordnick \
m_dnsbl \
m_eventexec \
m_exemptchanops \
m_exmode \
m_extbanbanlist \
m_extbanredirect \
m_extbanregex \
m_fakelist \
m_filter \
m_flashpolicyd \
m_forceident \
m_gecosban \
m_geoban \
m_geoclass \
m_geocmd \
m_geo_maxmind \
m_globalload \
m_globalmessageflood \
m_globops \
m_haproxy \
m_helpop \
m_hidechans \
m_hideidle \
m_hidelist \
m_hidemode \
m_hideoper \
m_hostchange \
m_hostcycle \
m_httpd \
m_httpd_acl \
m_httpd_config \
m_httpd_stats \
m_ident \
m_identmeta \
m_inviteexception \
m_ircv3 \
m_ircv3_accounttag \
m_ircv3_batch \
m_ircv3_capnotify \
m_ircv3_chghost \
m_ircv3_ctctags \
m_ircv3_echomessage \
m_ircv3_invitenotify \
m_ircv3_labeledresponse \
m_ircv3_msgid \
m_ircv3_servertime \
m_ircv3_sts \
m_join0 \
m_joinflood \
m_joinpartsno \
m_joinpartspam \
m_jumpserver \
m_kicknorejoin \
m_kill_idle \
m_knock \
m_lockserv \
m_maphide \
m_md5 \
m_messageflood \
m_messagelength \
m_mlock \
m_modenotice \
m_monitor \
m_muteban \
m_namedmodes \
m_namedstats \
m_namesx \
m_nationalchars \
m_nickdelay \
m_nickflood \
m_nicklock \
m_nocreate \
m_noctcp \
m_noidletyping \
m_nokicks \
m_nonicks \
m_nonotice \
m_nopartmsg \
m_noprivatemode \
m_nouidnick \
m_ojoin \
m_operchans \
m_operjoin \
m_operlevels \
m_operlog \
m_opermodes \
m_opermotd \
m_operprefix \
m_opmoderated \
m_override \
m_owoifier \
m_passforward \
m_password_hash \
m_pbkdf2 \
m_permchannels \
m_qrcode \
m_randomnotice \
m_randquote \
m_redirect \
m_relaymsg \
m_remove \
m_repeat \
m_require_auth \
m_restrictchans \
m_restrictmsg \
m_restrictmsg_duration \
m_rline \
m_rmode \
m_roleplay \
m_rotatelog \
m_sajoin \
m_sakick \
m_samode \
m_samove \
m_sanick \
m_sapart \
m_saquit \
m_sasl \
m_satopic \
m_securelist \
m_seenicks \
m_serverban \
m_services_account \
m_servprotect \
m_sethost \
m_setident \
m_setidle \
m_setname \
m_sha1 \
m_sha256 \
m_sha512 \
m_shed_users \
m_showfile \
m_showwhois \
m_shun \
m_silence \
m_slowmode \
m_solvemsg \
m_sqlauth \
m_sqloper \
m_sslinfo \
m_sslmodes \
m_ssl_openssl \
m_sslrehashsignal \
m_starttls \
m_stats_unlinked \
m_stripcolor \
m_svshold \
m_svsoper \
m_swhois \
m_tag_iphost \
m_teams \
m_telegraf \
m_teststdrpl \
m_tgchange \
m_timedbans \
m_timedstaticquit \
m_tline \
m_topiclock \
m_totp \
m_uhnames \
m_uninvite \
m_userip \
m_vhost \
m_watch \
m_websocket \
m_xline_db \
m_xlinetools

m_spanningtree.subdir = src/m_spanningtree/
m_zombie.subdir = src/m_zombie/
m_abbreviation.subdir = src/m_abbreviation/
m_alias.subdir = src/m_alias/
m_allowinvite.subdir = src/m_allowinvite/
m_alltime.subdir = src/m_alltime/
m_anticaps.subdir = src/m_anticaps/
m_antirandom.subdir = src/m_antirandom/
m_argon2.subdir = src/m_argon2/
m_asciiswitch.subdir = src/m_asciiswitch/
m_asn.subdir = src/m_asn/
m_auditorium.subdir = src/m_auditorium/
m_autoaway.subdir = src/m_autoaway/
m_autodrop.subdir = src/m_autodrop/
m_autokick.subdir = src/m_autokick/
m_autoop.subdir = src/m_autoop/
m_banexception.subdir = src/m_banexception/
m_bannegate.subdir = src/m_bannegate/
m_banredirect.subdir = src/m_banredirect/
m_bcrypt.subdir = src/m_bcrypt/
m_blockamsg.subdir = src/m_blockamsg/
m_blockcaps.subdir = src/m_blockcaps/
m_blockcolor.subdir = src/m_blockcolor/
m_blockhighlight.subdir = src/m_blockhighlight/
m_blockinvite.subdir = src/m_blockinvite/
m_botmode.subdir = src/m_botmode/
m_callerid.subdir = src/m_callerid/
m_cap.subdir = src/m_cap/
m_cban.subdir = src/m_cban/
m_censor.subdir = src/m_censor/
m_cgiirc.subdir = src/m_cgiirc/
m_chancreate.subdir = src/m_chancreate/
m_chanfilter.subdir = src/m_chanfilter/
m_changecap.subdir = src/m_changecap/
m_chanhistory.subdir = src/m_chanhistory/
m_chanlog.subdir = src/m_chanlog/
m_channames.subdir = src/m_channames/
m_channelban.subdir = src/m_channelban/
m_check.subdir = src/m_check/
m_checkbans.subdir = src/m_checkbans/
m_chghost.subdir = src/m_chghost/
m_chgident.subdir = src/m_chgident/
m_chgname.subdir = src/m_chgname/
m_classban.subdir = src/m_classban/
m_clearchan.subdir = src/m_clearchan/
m_clientcheck.subdir = src/m_clientcheck/
m_cloaking.subdir = src/m_cloaking/
m_clones.subdir = src/m_clones/
m_close.subdir = src/m_close/
m_codepage.subdir = src/m_codepage/
m_commonchans.subdir = src/m_commonchans/
m_complete.subdir = src/m_complete/
m_conn_accounts.subdir = src/m_conn_accounts/
m_conn_banner.subdir = src/m_conn_banner/
m_connectban.subdir = src/m_connectban/
m_connflood.subdir = src/m_connflood/
m_conn_join.subdir = src/m_conn_join/
m_conn_join_geoip.subdir = src/m_conn_join_geoip/
m_conn_join_ident.subdir = src/m_conn_join_ident/
m_conn_matchident.subdir = src/m_conn_matchident/
m_conn_require.subdir = src/m_conn_require/
m_conn_strictsasl.subdir = src/m_conn_strictsasl/
m_conn_umodes.subdir = src/m_conn_umodes/
m_conn_vhost.subdir = src/m_conn_vhost/
m_conn_waitpong.subdir = src/m_conn_waitpong/
m_custompenalty.subdir = src/m_custompenalty/
m_customprefix.subdir = src/m_customprefix/
m_customtags.subdir = src/m_customtags/
m_customtitle.subdir = src/m_customtitle/
m_cycle.subdir = src/m_cycle/
m_dccallow.subdir = src/m_dccallow/
m_dccblock.subdir = src/m_dccblock/
m_deaf.subdir = src/m_deaf/
m_defaulttopic.subdir = src/m_defaulttopic/
m_delayjoin.subdir = src/m_delayjoin/
m_delaymsg.subdir = src/m_delaymsg/
m_denychans.subdir = src/m_denychans/
m_disable.subdir = src/m_disable/
m_discordnick.subdir = src/m_discordnick/
m_dnsbl.subdir = src/m_dnsbl/
m_eventexec.subdir = src/m_eventexec/
m_exemptchanops.subdir = src/m_exemptchanops/
m_exmode.subdir = src/m_exmode/
m_extbanbanlist.subdir = src/m_extbanbanlist/
m_extbanredirect.subdir = src/m_extbanredirect/
m_extbanregex.subdir = src/m_extbanregex/
m_fakelist.subdir = src/m_fakelist/
m_filter.subdir = src/m_filter/
m_flashpolicyd.subdir = src/m_flashpolicyd/
m_forceident.subdir = src/m_forceident/
m_gecosban.subdir = src/m_gecosban/
m_geoban.subdir = src/m_geoban/
m_geoclass.subdir = src/m_geoclass/
m_geocmd.subdir = src/m_geocmd/
m_geo_maxmind.subdir = src/m_geo_maxmind/
m_globalload.subdir = src/m_globalload/
m_globalmessageflood.subdir = src/m_globalmessageflood/
m_globops.subdir = src/m_globops/
m_haproxy.subdir = src/m_haproxy/
# m_hash_gnutls.subdir = src/m_hash_gnutls/
m_helpop.subdir = src/m_helpop/
m_hidechans.subdir = src/m_hidechans/
m_hideidle.subdir = src/m_hideidle/
m_hidelist.subdir = src/m_hidelist/
m_hidemode.subdir = src/m_hidemode/
m_hideoper.subdir = src/m_hideoper/
m_hostchange.subdir = src/m_hostchange/
m_hostcycle.subdir = src/m_hostcycle/
m_httpd.subdir = src/m_httpd/
m_httpd_acl.subdir = src/m_httpd_acl/
m_httpd_config.subdir = src/m_httpd_config/
m_httpd_stats.subdir = src/m_httpd_stats/
m_ident.subdir = src/m_ident/
m_identmeta.subdir = src/m_identmeta/
m_inviteexception.subdir = src/m_inviteexception/
m_ircv3.subdir = src/m_ircv3/
m_ircv3_accounttag.subdir = src/m_ircv3_accounttag/
m_ircv3_batch.subdir = src/m_ircv3_batch/
m_ircv3_capnotify.subdir = src/m_ircv3_capnotify/
m_ircv3_chghost.subdir = src/m_ircv3_chghost/
m_ircv3_ctctags.subdir = src/m_ircv3_ctctags/
m_ircv3_echomessage.subdir = src/m_ircv3_echomessage/
m_ircv3_invitenotify.subdir = src/m_ircv3_invitenotify/
m_ircv3_labeledresponse.subdir = src/m_ircv3_labeledresponse/
m_ircv3_msgid.subdir = src/m_ircv3_msgid/
m_ircv3_servertime.subdir = src/m_ircv3_servertime/
m_ircv3_sts.subdir = src/m_ircv3_sts/
m_join0.subdir = src/m_join0/
m_joinflood.subdir = src/m_joinflood/
m_joinpartsno.subdir = src/m_joinpartsno/
m_joinpartspam.subdir = src/m_joinpartspam/
m_jumpserver.subdir = src/m_jumpserver/
m_kicknorejoin.subdir = src/m_kicknorejoin/
m_kill_idle.subdir = src/m_kill_idle/
m_knock.subdir = src/m_knock/
# m_ldap.subdir = src/m_ldap/
# m_ldapauth.subdir = src/m_ldapauth/
# m_ldapoper.subdir = src/m_ldapoper/
m_lockserv.subdir = src/m_lockserv/
m_maphide.subdir = src/m_maphide/
m_md5.subdir = src/m_md5/
m_messageflood.subdir = src/m_messageflood/
m_messagelength.subdir = src/m_messagelength/
m_mlock.subdir = src/m_mlock/
m_modenotice.subdir = src/m_modenotice/
m_monitor.subdir = src/m_monitor/
m_muteban.subdir = src/m_muteban/
#m_mysql.subdir = src/m_mysql/
m_namedmodes.subdir = src/m_namedmodes/
m_namedstats.subdir = src/m_namedstats/
m_namesx.subdir = src/m_namesx/
m_nationalchars.subdir = src/m_nationalchars/
m_nickdelay.subdir = src/m_nickdelay/
m_nickflood.subdir = src/m_nickflood/
m_nicklock.subdir = src/m_nicklock/
m_nocreate.subdir = src/m_nocreate/
m_noctcp.subdir = src/m_noctcp/
m_noidletyping.subdir = src/m_noidletyping/
m_nokicks.subdir = src/m_nokicks/
m_nonicks.subdir = src/m_nonicks/
m_nonotice.subdir = src/m_nonotice/
m_nopartmsg.subdir = src/m_nopartmsg/
m_noprivatemode.subdir = src/m_noprivatemode/
m_nouidnick.subdir = src/m_nouidnick/
m_ojoin.subdir = src/m_ojoin/
m_operchans.subdir = src/m_operchans/
m_operjoin.subdir = src/m_operjoin/
m_operlevels.subdir = src/m_operlevels/
m_operlog.subdir = src/m_operlog/
m_opermodes.subdir = src/m_opermodes/
m_opermotd.subdir = src/m_opermotd/
m_operprefix.subdir = src/m_operprefix/
m_opmoderated.subdir = src/m_opmoderated/
m_override.subdir = src/m_override/
m_owoifier.subdir = src/m_owoifier/
m_passforward.subdir = src/m_passforward/
m_password_hash.subdir = src/m_password_hash/
m_pbkdf2.subdir = src/m_pbkdf2/
m_permchannels.subdir = src/m_permchannels/
#m_pgsql.subdir = src/m_pgsql/
m_qrcode.subdir = src/m_qrcode/
m_randomnotice.subdir = src/m_randomnotice/
m_randquote.subdir = src/m_randquote/
m_redirect.subdir = src/m_redirect/
#m_regex_glob.subdir = src/m_regex_glob/
#m_regex_pcre.subdir = src/m_regex_pcre/
#m_regex_pcre2.subdir = src/m_regex_pcre2/
#m_regex_posix.subdir = src/m_regex_posix/
#m_regex_re2.subdir = src/m_regex_re2/
#m_regex_stdlib.subdir = src/m_regex_stdlib/
#m_regex_tre.subdir = src/m_regex_tre/
m_relaymsg.subdir = src/m_relaymsg/
m_remove.subdir = src/m_remove/
m_repeat.subdir = src/m_repeat/
m_require_auth.subdir = src/m_require_auth/
m_restrictchans.subdir = src/m_restrictchans/
m_restrictmsg.subdir = src/m_restrictmsg/
m_restrictmsg_duration.subdir = src/m_restrictmsg_duration/
m_rline.subdir = src/m_rline/
m_rmode.subdir = src/m_rmode/
m_roleplay.subdir = src/m_roleplay/
m_rotatelog.subdir = src/m_rotatelog/
m_sajoin.subdir = src/m_sajoin/
m_sakick.subdir = src/m_sakick/
m_samode.subdir = src/m_samode/
m_samove.subdir = src/m_samove/
m_sanick.subdir = src/m_sanick/
m_sapart.subdir = src/m_sapart/
m_saquit.subdir = src/m_saquit/
m_sasl.subdir = src/m_sasl/
m_satopic.subdir = src/m_satopic/
m_securelist.subdir = src/m_securelist/
m_seenicks.subdir = src/m_seenicks/
m_serverban.subdir = src/m_serverban/
m_services_account.subdir = src/m_services_account/
m_servprotect.subdir = src/m_servprotect/
m_sethost.subdir = src/m_sethost/
m_setident.subdir = src/m_setident/
m_setidle.subdir = src/m_setidle/
m_setname.subdir = src/m_setname/
m_sha1.subdir = src/m_sha1/
m_sha256.subdir = src/m_sha256/
m_sha512.subdir = src/m_sha512/
m_shed_users.subdir = src/m_shed_users/
m_showfile.subdir = src/m_showfile/
m_showwhois.subdir = src/m_showwhois/
m_shun.subdir = src/m_shun/
m_silence.subdir = src/m_silence/
m_slowmode.subdir = src/m_slowmode/
m_solvemsg.subdir = src/m_solvemsg/
m_sqlauth.subdir = src/m_sqlauth/
#m_sqlite3.subdir = src/m_sqlite3/
m_sqloper.subdir = src/m_sqloper/
# m_ssl_gnutls.subdir = src/m_ssl_gnutls/
m_sslinfo.subdir = src/m_sslinfo/
#m_ssl_mbedtls.subdir = src/m_ssl_mbedtls/
m_sslmodes.subdir = src/m_sslmodes/
m_ssl_openssl.subdir = src/m_ssl_openssl/
m_sslrehashsignal.subdir = src/m_sslrehashsignal/
m_starttls.subdir = src/m_starttls/
m_stats_unlinked.subdir = src/m_stats_unlinked/
m_stripcolor.subdir = src/m_stripcolor/
m_svshold.subdir = src/m_svshold/
m_svsoper.subdir = src/m_svsoper/
m_swhois.subdir = src/m_swhois/
m_tag_iphost.subdir = src/m_tag_iphost/
m_teams.subdir = src/m_teams/
m_telegraf.subdir = src/m_telegraf/
m_teststdrpl.subdir = src/m_teststdrpl/
m_tgchange.subdir = src/m_tgchange/
m_timedbans.subdir = src/m_timedbans/
m_timedstaticquit.subdir = src/m_timedstaticquit/
m_tline.subdir = src/m_tline/
m_topiclock.subdir = src/m_topiclock/
m_totp.subdir = src/m_totp/
m_uhnames.subdir = src/m_uhnames/
m_uninvite.subdir = src/m_uninvite/
m_userip.subdir = src/m_userip/
m_vhost.subdir = src/m_vhost/
m_watch.subdir = src/m_watch/
m_websocket.subdir = src/m_websocket/
m_xline_db.subdir = src/m_xline_db/
m_xlinetools.subdir = src/m_xlinetools/
