TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_inviteexception.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_inviteexception/

TARGET = ../../../out/lib/clandestine/modules/m_inviteexception

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
