TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_chghost.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_chghost/

TARGET = ../../../out/lib/clandestine/modules/m_chghost

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
