TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_gecosban.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_gecosban/

TARGET = ../../../out/lib/clandestine/modules/m_gecosban

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
