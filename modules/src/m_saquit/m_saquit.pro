TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_saquit.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_saquit/

TARGET = ../../../out/lib/clandestine/modules/m_saquit

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
