TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_asciiswitch.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_asciiswitch/

TARGET = ../../../out/lib/clandestine/modules/m_asciiswitch

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
