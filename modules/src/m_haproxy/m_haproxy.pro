TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_haproxy.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_haproxy/

TARGET = ../../../out/lib/clandestine/modules/m_haproxy

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
