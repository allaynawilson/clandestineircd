TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_services_account.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_services_account/

TARGET = ../../../out/lib/clandestine/modules/m_services_account

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
