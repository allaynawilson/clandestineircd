TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_namedstats.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_namedstats/

TARGET = ../../../out/lib/clandestine/modules/m_namedstats

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
