TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_rline.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_rline/

TARGET = ../../../out/lib/clandestine/modules/m_rline

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
