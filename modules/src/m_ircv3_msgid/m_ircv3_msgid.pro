TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_ircv3_msgid.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_ircv3_msgid/

TARGET = ../../../out/lib/clandestine/modules/m_ircv3_msgid

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
