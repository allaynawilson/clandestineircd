TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_zombie.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_zombie/

TARGET = ../../../out/lib/clandestine/modules/m_zombie

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/

DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
