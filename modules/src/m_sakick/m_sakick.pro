TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sakick.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sakick/

TARGET = ../../../out/lib/clandestine/modules/m_sakick

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
