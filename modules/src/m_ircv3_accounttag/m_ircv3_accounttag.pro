TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_ircv3_accounttag.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_ircv3_accounttag/

TARGET = ../../../out/lib/clandestine/modules/m_ircv3_accounttag

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
