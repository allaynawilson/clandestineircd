TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_close.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_close/

TARGET = ../../../out/lib/clandestine/modules/m_close

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
