TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_modenotice.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_modenotice/

TARGET = ../../../out/lib/clandestine/modules/m_modenotice

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
