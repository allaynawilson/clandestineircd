TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_filter.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_filter/

TARGET = ../../../out/lib/clandestine/modules/m_filter

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
