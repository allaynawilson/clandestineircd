TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_joinpartsno.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_joinpartsno/

TARGET = ../../../out/lib/clandestine/modules/m_joinpartsno

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
