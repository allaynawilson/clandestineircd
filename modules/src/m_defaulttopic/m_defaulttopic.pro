TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_defaulttopic.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_defaulttopic/

TARGET = ../../../out/lib/clandestine/modules/m_defaulttopic

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
