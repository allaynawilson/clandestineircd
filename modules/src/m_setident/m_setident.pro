TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_setident.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_setident/

TARGET = ../../../out/lib/clandestine/modules/m_setident

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
