TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_repeat.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_repeat/

TARGET = ../../../out/lib/clandestine/modules/m_repeat

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
