TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_samove.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_samove/

TARGET = ../../../out/lib/clandestine/modules/m_samove

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
