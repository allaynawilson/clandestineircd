TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_delayjoin.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_delayjoin/

TARGET = ../../../out/lib/clandestine/modules/m_delayjoin

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
