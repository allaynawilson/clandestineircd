TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_remove.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_remove/

TARGET = ../../../out/lib/clandestine/modules/m_remove

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
