TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
data-pool.c \
maxminddb.c \
m_geo_maxmind.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_geo_maxmind/

TARGET = ../../../out/lib/clandestine/modules/m_geo_maxmind

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
