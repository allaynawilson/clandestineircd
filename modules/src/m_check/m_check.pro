TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_check.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_check/

TARGET = ../../../out/lib/clandestine/modules/m_check

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
