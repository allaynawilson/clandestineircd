TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_checkbans.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_checkbans/

TARGET = ../../../out/lib/clandestine/modules/m_checkbans

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
