TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_tline.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_tline/

TARGET = ../../../out/lib/clandestine/modules/m_tline

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
