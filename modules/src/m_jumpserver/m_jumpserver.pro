TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_jumpserver.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_jumpserver/

TARGET = ../../../out/lib/clandestine/modules/m_jumpserver

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
