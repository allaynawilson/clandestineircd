TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_opmoderated.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_opmoderated/

TARGET = ../../../out/lib/clandestine/modules/m_opmoderated

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
