TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_clones.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_clones/

TARGET = ../../../out/lib/clandestine/modules/m_clones

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
