TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_stripcolor.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_stripcolor/

TARGET = ../../../out/lib/clandestine/modules/m_stripcolor

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
