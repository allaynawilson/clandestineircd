TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_randquote.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_randquote/

TARGET = ../../../out/lib/clandestine/modules/m_randquote

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
