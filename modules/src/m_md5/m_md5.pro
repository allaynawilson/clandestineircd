TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_md5.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_md5/

TARGET = ../../../out/lib/clandestine/modules/m_md5

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
