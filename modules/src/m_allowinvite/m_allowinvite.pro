TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_allowinvite.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_allowinvite/

TARGET = ../../../out/lib/clandestine/modules/m_allowinvite

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
