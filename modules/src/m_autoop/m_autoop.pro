TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_autoop.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_autoop/

TARGET = ../../../out/lib/clandestine/modules/m_autoop

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
