TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_override.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_override/

TARGET = ../../../out/lib/clandestine/modules/m_override

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
