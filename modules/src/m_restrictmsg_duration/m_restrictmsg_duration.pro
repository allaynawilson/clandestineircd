TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_restrictmsg_duration.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_restrictmsg_duration/

TARGET = ../../../out/lib/clandestine/modules/m_restrictmsg_duration

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
