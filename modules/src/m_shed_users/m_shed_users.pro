TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_shed_users.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_shed_users/

TARGET = ../../../out/lib/clandestine/modules/m_shed_users

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
