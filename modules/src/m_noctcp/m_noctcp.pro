TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_noctcp.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_noctcp/

TARGET = ../../../out/lib/clandestine/modules/m_noctcp

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
