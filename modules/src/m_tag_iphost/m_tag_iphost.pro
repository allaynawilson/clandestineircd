TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_tag_iphost.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_tag_iphost/

TARGET = ../../../out/lib/clandestine/modules/m_tag_iphost

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
