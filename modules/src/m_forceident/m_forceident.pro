TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_forceident.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_forceident/

TARGET = ../../../out/lib/clandestine/modules/m_forceident

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
