TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_customtags.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_customtags/

TARGET = ../../../out/lib/clandestine/modules/m_customtags

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
