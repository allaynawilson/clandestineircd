TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_argon2.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_argon2/

TARGET = ../../../out/lib/clandestine/modules/m_argon2

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/

LIBS += -largon2
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
