TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_cap.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_cap/

TARGET = ../../../out/lib/clandestine/modules/m_cap

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
