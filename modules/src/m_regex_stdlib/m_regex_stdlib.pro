TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_regex_stdlib.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_regex_stdlib/

TARGET = ../../../out/lib/clandestine/modules/m_regex_stdlib

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
