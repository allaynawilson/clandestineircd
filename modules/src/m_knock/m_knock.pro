TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_knock.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_knock/

TARGET = ../../../out/lib/clandestine/modules/m_knock

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
