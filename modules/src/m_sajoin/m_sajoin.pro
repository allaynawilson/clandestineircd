TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sajoin.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sajoin/

TARGET = ../../../out/lib/clandestine/modules/m_sajoin

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
