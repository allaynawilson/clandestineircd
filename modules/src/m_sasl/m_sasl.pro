TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sasl.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sasl/

TARGET = ../../../out/lib/clandestine/modules/m_sasl

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
