TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_clientcheck.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_clientcheck/

TARGET = ../../../out/lib/clandestine/modules/m_clientcheck

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
