TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_setname.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_setname/

TARGET = ../../../out/lib/clandestine/modules/m_setname

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
