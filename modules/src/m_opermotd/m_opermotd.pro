TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_opermotd.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_opermotd/

TARGET = ../../../out/lib/clandestine/modules/m_opermotd

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
