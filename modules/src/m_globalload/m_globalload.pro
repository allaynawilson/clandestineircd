TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_globalload.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_globalload/

TARGET = ../../../out/lib/clandestine/modules/m_globalload

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
