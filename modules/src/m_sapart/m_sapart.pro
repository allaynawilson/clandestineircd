TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sapart.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sapart/

TARGET = ../../../out/lib/clandestine/modules/m_sapart

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
