TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_codepage.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_codepage/

TARGET = ../../../out/lib/clandestine/modules/m_codepage

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
