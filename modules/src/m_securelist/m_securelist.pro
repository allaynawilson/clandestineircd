TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_securelist.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_securelist/

TARGET = ../../../out/lib/clandestine/modules/m_securelist

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
