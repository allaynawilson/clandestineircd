TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_monitor.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_monitor/

TARGET = ../../../out/lib/clandestine/modules/m_monitor

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
