TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_hostchange.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_hostchange/

TARGET = ../../../out/lib/clandestine/modules/m_hostchange

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
