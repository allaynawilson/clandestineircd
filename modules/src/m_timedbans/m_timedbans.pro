TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_timedbans.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_timedbans/

TARGET = ../../../out/lib/clandestine/modules/m_timedbans

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
