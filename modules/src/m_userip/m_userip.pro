TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_userip.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_userip/

TARGET = ../../../out/lib/clandestine/modules/m_userip

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
