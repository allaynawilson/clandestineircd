TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_operprefix.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_operprefix/

TARGET = ../../../out/lib/clandestine/modules/m_operprefix

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
