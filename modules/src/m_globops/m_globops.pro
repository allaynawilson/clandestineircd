TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_globops.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_globops/

TARGET = ../../../out/lib/clandestine/modules/m_globops

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
