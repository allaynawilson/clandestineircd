TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_nationalchars.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_nationalchars/

TARGET = ../../../out/lib/clandestine/modules/m_nationalchars

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
