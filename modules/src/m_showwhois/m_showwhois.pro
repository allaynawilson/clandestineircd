TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_showwhois.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_showwhois/

TARGET = ../../../out/lib/clandestine/modules/m_showwhois

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
