TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_regex_re2.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_regex_re2/

TARGET = ../../../out/lib/clandestine/modules/m_regex_re2

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
