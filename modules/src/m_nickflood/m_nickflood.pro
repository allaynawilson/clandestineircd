TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_nickflood.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_nickflood/

TARGET = ../../../out/lib/clandestine/modules/m_nickflood

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
