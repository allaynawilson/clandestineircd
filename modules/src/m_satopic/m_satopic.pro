TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_satopic.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_satopic/

TARGET = ../../../out/lib/clandestine/modules/m_satopic

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
