TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_changecap.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_changecap/

TARGET = ../../../out/lib/clandestine/modules/m_changecap

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
