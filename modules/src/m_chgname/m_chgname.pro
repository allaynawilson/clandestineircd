TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_chgname.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_chgname/

TARGET = ../../../out/lib/clandestine/modules/m_chgname

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
