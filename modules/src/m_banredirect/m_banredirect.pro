TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_banredirect.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_banredirect/

TARGET = ../../../out/lib/clandestine/modules/m_banredirect

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
