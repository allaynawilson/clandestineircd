TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sslmodes.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sslmodes/

TARGET = ../../../out/lib/clandestine/modules/m_sslmodes

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
