TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_discordnick.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_discordnick/

TARGET = ../../../out/lib/clandestine/modules/m_discordnick

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
