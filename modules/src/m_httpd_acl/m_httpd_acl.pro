TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_httpd_acl.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_httpd_acl/

TARGET = ../../../out/lib/clandestine/modules/m_httpd_acl

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
