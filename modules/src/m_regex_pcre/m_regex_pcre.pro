TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_regex_pcre.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_regex_pcre/

TARGET = ../../../out/lib/clandestine/modules/m_regex_pcre

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
