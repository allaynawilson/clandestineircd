TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_silence.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_silence/

TARGET = ../../../out/lib/clandestine/modules/m_silence

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
