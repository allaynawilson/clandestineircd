TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_relaymsg.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_relaymsg/

TARGET = ../../../out/lib/clandestine/modules/m_relaymsg

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
