TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_stats_unlinked.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_stats_unlinked/

TARGET = ../../../out/lib/clandestine/modules/m_stats_unlinked

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
