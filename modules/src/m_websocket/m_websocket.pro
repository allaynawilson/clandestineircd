TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_websocket.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_websocket/

TARGET = ../../../out/lib/clandestine/modules/m_websocket

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
