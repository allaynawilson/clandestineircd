TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_flashpolicyd.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_flashpolicyd/

TARGET = ../../../out/lib/clandestine/modules/m_flashpolicyd

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
