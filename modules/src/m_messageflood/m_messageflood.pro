TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_messageflood.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_messageflood/

TARGET = ../../../out/lib/clandestine/modules/m_messageflood

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
