TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_blockinvite.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_blockinvite/

TARGET = ../../../out/lib/clandestine/modules/m_blockinvite

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
