TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_join0.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_join0/

TARGET = ../../../out/lib/clandestine/modules/m_join0

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
