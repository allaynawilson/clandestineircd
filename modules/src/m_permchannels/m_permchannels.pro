TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_permchannels.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_permchannels/

TARGET = ../../../out/lib/clandestine/modules/m_permchannels

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
