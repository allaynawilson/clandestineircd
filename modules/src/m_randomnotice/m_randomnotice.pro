TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_randomnotice.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_randomnotice/

TARGET = ../../../out/lib/clandestine/modules/m_randomnotice

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
