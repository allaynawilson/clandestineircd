TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_timedstaticquit.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_timedstaticquit/

TARGET = ../../../out/lib/clandestine/modules/m_timedstaticquit

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
