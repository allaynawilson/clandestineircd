TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_delaymsg.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_delaymsg/

TARGET = ../../../out/lib/clandestine/modules/m_delaymsg

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
