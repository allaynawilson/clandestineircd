TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_require_auth.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_require_auth/

TARGET = ../../../out/lib/clandestine/modules/m_require_auth

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
