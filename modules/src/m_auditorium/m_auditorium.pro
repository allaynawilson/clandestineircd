TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_auditorium.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_auditorium/

TARGET = ../../../out/lib/clandestine/modules/m_auditorium

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
