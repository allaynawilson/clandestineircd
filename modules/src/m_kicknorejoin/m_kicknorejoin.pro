TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_kicknorejoin.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_kicknorejoin/

TARGET = ../../../out/lib/clandestine/modules/m_kicknorejoin

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
