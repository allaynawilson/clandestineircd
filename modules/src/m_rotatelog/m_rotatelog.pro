TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_rotatelog.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_rotatelog/

TARGET = ../../../out/lib/clandestine/modules/m_rotatelog

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
