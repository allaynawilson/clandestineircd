TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_blockcolor.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_blockcolor/

TARGET = ../../../out/lib/clandestine/modules/m_blockcolor

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
