TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_antirandom.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_antirandom/

TARGET = ../../../out/lib/clandestine/modules/m_antirandom

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
