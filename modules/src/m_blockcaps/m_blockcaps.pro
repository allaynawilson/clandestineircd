TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_blockcaps.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_blockcaps/

TARGET = ../../../out/lib/clandestine/modules/m_blockcaps

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
