TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sqlite3.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sqlite3/

TARGET = ../../../out/lib/clandestine/modules/m_sqlite3

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
