TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_geoban.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_geoban/

TARGET = ../../../out/lib/clandestine/modules/m_geoban

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
