TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_nocreate.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_nocreate/

TARGET = ../../../out/lib/clandestine/modules/m_nocreate

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
