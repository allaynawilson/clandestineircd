TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
addline.cpp \
away.cpp \
capab.cpp \
compat.cpp \
delline.cpp \
encap.cpp \
fjoin.cpp \
fmode.cpp \
ftopic.cpp \
hmac.cpp \
idle.cpp \
ijoin.cpp \
main.cpp \
metadata.cpp \
misccommands.cpp \
netburst.cpp \
nickcollide.cpp \
nick.cpp \
num.cpp \
opertype.cpp \
override_map.cpp \
override_squit.cpp \
override_stats.cpp \
override_whois.cpp \
ping.cpp \
pingtimer.cpp \
pong.cpp \
postcommand.cpp \
precommand.cpp \
protocolinterface.cpp \
rconnect.cpp \
remoteuser.cpp \
resolvers.cpp \
rsquit.cpp \
save.cpp \
servercommand.cpp \
server.cpp \
sinfo.cpp \
svsjoin.cpp \
svsnick.cpp \
svspart.cpp \
tags.cpp \
translate.cpp \
treeserver.cpp \
treesocket1.cpp \
treesocket2.cpp \
uid.cpp \
utils.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_spanningtree/

TARGET = ../../../out/lib/clandestine/modules/m_spanningtree

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/

DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
