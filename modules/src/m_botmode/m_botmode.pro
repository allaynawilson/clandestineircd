TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_botmode.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_botmode/

TARGET = ../../../out/lib/clandestine/modules/m_botmode

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
