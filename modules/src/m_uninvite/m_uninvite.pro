TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_uninvite.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_uninvite/

TARGET = ../../../out/lib/clandestine/modules/m_uninvite

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
