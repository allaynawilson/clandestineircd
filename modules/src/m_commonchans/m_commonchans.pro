TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_commonchans.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_commonchans/

TARGET = ../../../out/lib/clandestine/modules/m_commonchans

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
