TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_maphide.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_maphide/

TARGET = ../../../out/lib/clandestine/modules/m_maphide

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
