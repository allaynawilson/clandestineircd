TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_ldap.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_ldap/

TARGET = ../../../out/lib/clandestine/modules/m_ldap

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
