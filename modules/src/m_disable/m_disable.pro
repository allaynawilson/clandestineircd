TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_disable.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_disable/

TARGET = ../../../out/lib/clandestine/modules/m_disable

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
