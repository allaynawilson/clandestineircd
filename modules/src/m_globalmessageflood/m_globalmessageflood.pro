TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_globalmessageflood.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_globalmessageflood/

TARGET = ../../../out/lib/clandestine/modules/m_globalmessageflood

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
