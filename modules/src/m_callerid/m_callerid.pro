TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_callerid.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_callerid/

TARGET = ../../../out/lib/clandestine/modules/m_callerid

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
