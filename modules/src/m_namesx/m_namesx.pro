TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_namesx.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_namesx/

TARGET = ../../../out/lib/clandestine/modules/m_namesx

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
