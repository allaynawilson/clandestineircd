TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_banexception.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_banexception/

TARGET = ../../../out/lib/clandestine/modules/m_banexception

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
