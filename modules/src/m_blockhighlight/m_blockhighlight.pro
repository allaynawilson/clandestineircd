TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_blockhighlight.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_blockhighlight/

TARGET = ../../../out/lib/clandestine/modules/m_blockhighlight

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
