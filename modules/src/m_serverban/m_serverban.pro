TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_serverban.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_serverban/

TARGET = ../../../out/lib/clandestine/modules/m_serverban

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
