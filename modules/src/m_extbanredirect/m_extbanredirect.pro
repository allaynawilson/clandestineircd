TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_extbanredirect.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_extbanredirect/

TARGET = ../../../out/lib/clandestine/modules/m_extbanredirect

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
