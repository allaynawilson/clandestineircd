TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_eventexec.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_eventexec/

TARGET = ../../../out/lib/clandestine/modules/m_eventexec

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
