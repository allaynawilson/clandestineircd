TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_svshold.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_svshold/

TARGET = ../../../out/lib/clandestine/modules/m_svshold

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
