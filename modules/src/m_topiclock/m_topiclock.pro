TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_topiclock.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_topiclock/

TARGET = ../../../out/lib/clandestine/modules/m_topiclock

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
