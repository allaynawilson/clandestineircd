TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_mlock.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_mlock/

TARGET = ../../../out/lib/clandestine/modules/m_mlock

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
