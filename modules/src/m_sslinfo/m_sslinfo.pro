TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sslinfo.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sslinfo/

TARGET = ../../../out/lib/clandestine/modules/m_sslinfo

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
