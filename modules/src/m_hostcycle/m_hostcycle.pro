TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_hostcycle.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_hostcycle/

TARGET = ../../../out/lib/clandestine/modules/m_hostcycle

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
