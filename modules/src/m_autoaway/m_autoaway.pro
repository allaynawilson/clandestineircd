TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_autoaway.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_autoaway/

TARGET = ../../../out/lib/clandestine/modules/m_autoaway

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
