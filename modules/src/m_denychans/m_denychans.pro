TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_denychans.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_denychans/

TARGET = ../../../out/lib/clandestine/modules/m_denychans

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
