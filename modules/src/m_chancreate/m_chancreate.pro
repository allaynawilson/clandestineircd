TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_chancreate.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_chancreate/

TARGET = ../../../out/lib/clandestine/modules/m_chancreate

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
