TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_tgchange.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_tgchange/

TARGET = ../../../out/lib/clandestine/modules/m_tgchange

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
