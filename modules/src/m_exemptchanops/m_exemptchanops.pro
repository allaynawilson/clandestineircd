TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_exemptchanops.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_exemptchanops/

TARGET = ../../../out/lib/clandestine/modules/m_exemptchanops

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
