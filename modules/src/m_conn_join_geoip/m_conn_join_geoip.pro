TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_conn_join_geoip.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_conn_join_geoip/

TARGET = ../../../out/lib/clandestine/modules/m_conn_join_geoip

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
