TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_nicklock.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_nicklock/

TARGET = ../../../out/lib/clandestine/modules/m_nicklock

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
