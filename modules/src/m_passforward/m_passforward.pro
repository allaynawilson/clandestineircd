TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_passforward.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_passforward/

TARGET = ../../../out/lib/clandestine/modules/m_passforward

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
