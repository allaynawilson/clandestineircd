TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_operjoin.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_operjoin/

TARGET = ../../../out/lib/clandestine/modules/m_operjoin

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
