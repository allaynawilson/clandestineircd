TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_customprefix.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_customprefix/

TARGET = ../../../out/lib/clandestine/modules/m_customprefix

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
