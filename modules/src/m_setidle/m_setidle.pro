TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_setidle.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_setidle/

TARGET = ../../../out/lib/clandestine/modules/m_setidle

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
