TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_chanlog.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_chanlog/

TARGET = ../../../out/lib/clandestine/modules/m_chanlog

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
