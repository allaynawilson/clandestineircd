TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_operlevels.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_operlevels/

TARGET = ../../../out/lib/clandestine/modules/m_operlevels

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
