TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sslrehashsignal.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sslrehashsignal/

TARGET = ../../../out/lib/clandestine/modules/m_sslrehashsignal

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
