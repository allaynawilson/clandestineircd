TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_xlinetools.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_xlinetools/

TARGET = ../../../out/lib/clandestine/modules/m_xlinetools

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
