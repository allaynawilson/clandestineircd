TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_ssl_openssl.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_ssl_openssl/

TARGET = ../../../out/lib/clandestine/modules/m_ssl_openssl

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
