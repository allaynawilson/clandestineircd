TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_noprivatemode.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_noprivatemode/

TARGET = ../../../out/lib/clandestine/modules/m_noprivatemode

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
