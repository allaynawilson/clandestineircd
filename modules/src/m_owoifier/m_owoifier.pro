TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_owoifier.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_owoifier/

TARGET = ../../../out/lib/clandestine/modules/m_owoifier

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
