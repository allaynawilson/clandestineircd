TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_abbreviation.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_abbreviation/

TARGET = ../../../out/lib/clandestine/modules/m_abbreviation

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
