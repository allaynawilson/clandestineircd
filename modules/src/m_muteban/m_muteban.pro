TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_muteban.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_muteban/

TARGET = ../../../out/lib/clandestine/modules/m_muteban

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
