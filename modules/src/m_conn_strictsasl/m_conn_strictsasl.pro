TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_conn_strictsasl.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_conn_strictsasl/

TARGET = ../../../out/lib/clandestine/modules/m_conn_strictsasl

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
