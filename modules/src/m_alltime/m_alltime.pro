TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_alltime.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_alltime/

TARGET = ../../../out/lib/clandestine/modules/m_alltime

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
