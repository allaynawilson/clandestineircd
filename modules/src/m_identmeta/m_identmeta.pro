TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_identmeta.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_identmeta/

TARGET = ../../../out/lib/clandestine/modules/m_identmeta

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
