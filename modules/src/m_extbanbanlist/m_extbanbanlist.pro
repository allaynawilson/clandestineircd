TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_extbanbanlist.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_extbanbanlist/

TARGET = ../../../out/lib/clandestine/modules/m_extbanbanlist

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
