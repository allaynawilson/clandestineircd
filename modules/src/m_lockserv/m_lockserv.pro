TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_lockserv.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_lockserv/

TARGET = ../../../out/lib/clandestine/modules/m_lockserv

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
