TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_channames.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_channames/

TARGET = ../../../out/lib/clandestine/modules/m_channames

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
