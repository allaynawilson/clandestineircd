TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_pgsql.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_pgsql/

TARGET = ../../../out/lib/clandestine/modules/m_pgsql

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
