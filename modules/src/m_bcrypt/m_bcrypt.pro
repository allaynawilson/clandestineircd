TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
crypt_blowfish.c \
crypt_gensalt.c \
wrapper.c \
m_bcrypt.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_bcrypt/

TARGET = ../../../out/lib/clandestine/modules/m_bcrypt

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
