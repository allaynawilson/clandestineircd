TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_asn.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_asn/

TARGET = ../../../out/lib/clandestine/modules/m_asn

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
