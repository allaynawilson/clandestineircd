TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_chgident.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_chgident/

TARGET = ../../../out/lib/clandestine/modules/m_chgident

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
