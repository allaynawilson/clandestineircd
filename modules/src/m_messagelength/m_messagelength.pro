TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_messagelength.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_messagelength/

TARGET = ../../../out/lib/clandestine/modules/m_messagelength

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
