TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_ssl_gnutls.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_ssl_gnutls/

TARGET = ../../../out/lib/clandestine/modules/m_ssl_gnutls

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
