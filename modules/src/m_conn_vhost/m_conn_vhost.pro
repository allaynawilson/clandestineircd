TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_conn_vhost.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_conn_vhost/

TARGET = ../../../out/lib/clandestine/modules/m_conn_vhost

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
