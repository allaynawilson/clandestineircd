TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_dccblock.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_dccblock/

TARGET = ../../../out/lib/clandestine/modules/m_dccblock

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
