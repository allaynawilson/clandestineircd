TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_nokicks.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_nokicks/

TARGET = ../../../out/lib/clandestine/modules/m_nokicks

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
