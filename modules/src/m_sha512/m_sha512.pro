TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sha512.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sha512/

TARGET = ../../../out/lib/clandestine/modules/m_sha512

INCLUDEPATH += ../m_sha256

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/

DEFINES += SHA_64BIT

DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"

QMAKE_CXXFLAGS += -fpermissive
