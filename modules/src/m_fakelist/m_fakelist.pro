TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_fakelist.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_fakelist/

TARGET = ../../../out/lib/clandestine/modules/m_fakelist

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
