TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_shun.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_shun/

TARGET = ../../../out/lib/clandestine/modules/m_shun

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
