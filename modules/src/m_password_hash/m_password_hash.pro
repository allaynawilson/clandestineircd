TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_password_hash.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_password_hash/

TARGET = ../../../out/lib/clandestine/modules/m_password_hash

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
