TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_cloaking.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_cloaking/

TARGET = ../../../out/lib/clandestine/modules/m_cloaking

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
