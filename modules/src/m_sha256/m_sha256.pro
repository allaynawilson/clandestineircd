TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sha256.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sha256/

TARGET = ../../../out/lib/clandestine/modules/m_sha256

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/

DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"

DEFINES += SHA_64BIT

QMAKE_CXXFLAGS += -fpermissive
