TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_extbanregex.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_extbanregex/

TARGET = ../../../out/lib/clandestine/modules/m_extbanregex

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
