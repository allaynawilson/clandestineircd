TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_watch.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_watch/

TARGET = ../../../out/lib/clandestine/modules/m_watch

INCLUDEPATH += ../m_monitor

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
