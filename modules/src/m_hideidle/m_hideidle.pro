TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_hideidle.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_hideidle/

TARGET = ../../../out/lib/clandestine/modules/m_hideidle

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
