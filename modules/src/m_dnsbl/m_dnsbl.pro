TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_dnsbl.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_dnsbl/

TARGET = ../../../out/lib/clandestine/modules/m_dnsbl

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
