TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_ojoin.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_ojoin/

TARGET = ../../../out/lib/clandestine/modules/m_ojoin

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
