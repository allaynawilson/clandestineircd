TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_slowmode.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_slowmode/

TARGET = ../../../out/lib/clandestine/modules/m_slowmode

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
