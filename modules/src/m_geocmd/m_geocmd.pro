TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_geocmd.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_geocmd/

TARGET = ../../../out/lib/clandestine/modules/m_geocmd

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
