TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_chanhistory.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_chanhistory/

TARGET = ../../../out/lib/clandestine/modules/m_chanhistory

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
