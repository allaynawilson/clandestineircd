TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sethost.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sethost/

TARGET = ../../../out/lib/clandestine/modules/m_sethost

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
