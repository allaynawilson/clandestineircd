TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_custompenalty.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_custompenalty/

TARGET = ../../../out/lib/clandestine/modules/m_custompenalty

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
