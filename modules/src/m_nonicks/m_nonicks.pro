TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_nonicks.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_nonicks/

TARGET = ../../../out/lib/clandestine/modules/m_nonicks

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
