TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sha1.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sha1/

TARGET = ../../../out/lib/clandestine/modules/m_sha1

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
