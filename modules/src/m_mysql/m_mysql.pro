TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_mysql.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_mysql/

TARGET = ../../../out/lib/clandestine/modules/m_mysql

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
