TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_hideoper.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_hideoper/

TARGET = ../../../out/lib/clandestine/modules/m_hideoper

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
