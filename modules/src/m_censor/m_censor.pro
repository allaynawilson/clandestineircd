TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_censor.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_censor/

TARGET = ../../../out/lib/clandestine/modules/m_censor

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
