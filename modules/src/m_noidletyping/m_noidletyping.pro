TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_noidletyping.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_noidletyping/

TARGET = ../../../out/lib/clandestine/modules/m_noidletyping

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
