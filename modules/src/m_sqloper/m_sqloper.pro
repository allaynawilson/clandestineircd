TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sqloper.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sqloper/

TARGET = ../../../out/lib/clandestine/modules/m_sqloper

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
