TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_restrictchans.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_restrictchans/

TARGET = ../../../out/lib/clandestine/modules/m_restrictchans

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
