TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_hidemode.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_hidemode/

TARGET = ../../../out/lib/clandestine/modules/m_hidemode

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
