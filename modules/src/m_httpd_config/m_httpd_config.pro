TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_httpd_config.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_httpd_config/

TARGET = ../../../out/lib/clandestine/modules/m_httpd_config

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
