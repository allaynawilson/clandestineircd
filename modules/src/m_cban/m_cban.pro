TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_cban.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_cban/

TARGET = ../../../out/lib/clandestine/modules/m_cban

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
