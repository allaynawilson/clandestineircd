TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_bannegate.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_bannegate/

TARGET = ../../../out/lib/clandestine/modules/m_bannegate

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
