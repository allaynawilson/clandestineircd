TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_uhnames.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_uhnames/

TARGET = ../../../out/lib/clandestine/modules/m_uhnames

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
