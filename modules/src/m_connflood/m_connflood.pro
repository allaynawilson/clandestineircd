TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_connflood.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_connflood/

TARGET = ../../../out/lib/clandestine/modules/m_connflood

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
