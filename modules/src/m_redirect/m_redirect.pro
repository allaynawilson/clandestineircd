TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_redirect.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_redirect/

TARGET = ../../../out/lib/clandestine/modules/m_redirect

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
