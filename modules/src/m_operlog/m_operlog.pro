TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_operlog.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_operlog/

TARGET = ../../../out/lib/clandestine/modules/m_operlog

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
