TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_chanfilter.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_chanfilter/

TARGET = ../../../out/lib/clandestine/modules/m_chanfilter

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
