TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_httpd.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_httpd/

TARGET = ../../../out/lib/clandestine/modules/m_httpd

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
