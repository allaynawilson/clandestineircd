TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_deaf.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_deaf/

TARGET = ../../../out/lib/clandestine/modules/m_deaf

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
