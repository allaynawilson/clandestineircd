TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_ldapoper.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_ldapoper/

TARGET = ../../../out/lib/clandestine/modules/m_ldapoper

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
