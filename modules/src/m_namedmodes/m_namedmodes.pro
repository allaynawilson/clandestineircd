TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_namedmodes.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_namedmodes/

TARGET = ../../../out/lib/clandestine/modules/m_namedmodes

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
