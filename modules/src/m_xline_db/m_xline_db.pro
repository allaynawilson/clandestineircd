TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_xline_db.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_xline_db/

TARGET = ../../../out/lib/clandestine/modules/m_xline_db

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
