TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_showfile.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_showfile/

TARGET = ../../../out/lib/clandestine/modules/m_showfile

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
