TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_cycle.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_cycle/

TARGET = ../../../out/lib/clandestine/modules/m_cycle

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
