TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_kill_idle.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_kill_idle/

TARGET = ../../../out/lib/clandestine/modules/m_kill_idle

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
