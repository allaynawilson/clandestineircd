TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_nonotice.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_nonotice/

TARGET = ../../../out/lib/clandestine/modules/m_nonotice

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
