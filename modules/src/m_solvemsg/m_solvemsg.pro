TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_solvemsg.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_solvemsg/

TARGET = ../../../out/lib/clandestine/modules/m_solvemsg

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
