TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_nickdelay.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_nickdelay/

TARGET = ../../../out/lib/clandestine/modules/m_nickdelay

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
