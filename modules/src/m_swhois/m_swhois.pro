TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_swhois.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_swhois/

TARGET = ../../../out/lib/clandestine/modules/m_swhois

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
