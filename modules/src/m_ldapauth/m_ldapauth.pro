TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_ldapauth.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_ldapauth/

TARGET = ../../../out/lib/clandestine/modules/m_ldapauth

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
