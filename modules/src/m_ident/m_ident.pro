TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_ident.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_ident/

TARGET = ../../../out/lib/clandestine/modules/m_ident

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
