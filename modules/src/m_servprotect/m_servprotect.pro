TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_servprotect.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_servprotect/

TARGET = ../../../out/lib/clandestine/modules/m_servprotect

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
