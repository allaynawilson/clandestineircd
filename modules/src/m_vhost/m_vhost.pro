TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_vhost.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_vhost/

TARGET = ../../../out/lib/clandestine/modules/m_vhost

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
