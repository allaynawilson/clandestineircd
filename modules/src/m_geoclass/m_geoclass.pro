TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_geoclass.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_geoclass/

TARGET = ../../../out/lib/clandestine/modules/m_geoclass

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
