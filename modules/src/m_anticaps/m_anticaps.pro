TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_anticaps.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_anticaps/

TARGET = ../../../out/lib/clandestine/modules/m_anticaps

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
