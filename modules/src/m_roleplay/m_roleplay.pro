TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_roleplay.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_roleplay/

TARGET = ../../../out/lib/clandestine/modules/m_roleplay

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
