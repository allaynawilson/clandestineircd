TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_helpop.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_helpop/

TARGET = ../../../out/lib/clandestine/modules/m_helpop

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
