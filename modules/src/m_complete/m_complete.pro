TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_complete.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_complete/

TARGET = ../../../out/lib/clandestine/modules/m_complete

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
