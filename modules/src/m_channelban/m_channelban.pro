TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_channelban.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_channelban/

TARGET = ../../../out/lib/clandestine/modules/m_channelban

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
