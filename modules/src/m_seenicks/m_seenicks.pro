TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_seenicks.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_seenicks/

TARGET = ../../../out/lib/clandestine/modules/m_seenicks

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
