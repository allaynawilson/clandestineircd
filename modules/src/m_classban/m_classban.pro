TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_classban.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_classban/

TARGET = ../../../out/lib/clandestine/modules/m_classban

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
