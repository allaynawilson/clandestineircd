TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_hash_gnutls.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_hash_gnutls/

TARGET = ../../../out/lib/clandestine/modules/m_hash_gnutls

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
