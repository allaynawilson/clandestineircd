TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_samode.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_samode/

TARGET = ../../../out/lib/clandestine/modules/m_samode

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
