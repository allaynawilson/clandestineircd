TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_exmode.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_exmode/

TARGET = ../../../out/lib/clandestine/modules/m_exmode

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
