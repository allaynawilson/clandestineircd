TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_sanick.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_sanick/

TARGET = ../../../out/lib/clandestine/modules/m_sanick

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
