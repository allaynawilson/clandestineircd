TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_operchans.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_operchans/

TARGET = ../../../out/lib/clandestine/modules/m_operchans

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
