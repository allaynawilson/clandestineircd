TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_nopartmsg.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_nopartmsg/

TARGET = ../../../out/lib/clandestine/modules/m_nopartmsg

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
