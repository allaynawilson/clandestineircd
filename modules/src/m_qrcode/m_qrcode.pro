TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
bitstream.c \
mask.c \
mmask.c \
mqrspec.c \
qrenc.c \
qrencode.c \
qrinput.c \
qrspec.c \
rsecc.c \
split.c \
m_qrcode.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_qrcode/

TARGET = ../../../out/lib/clandestine/modules/m_qrcode

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/

DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"

DEFINES += MAJOR_VERSION=4

DEFINES += MICRO_VERSION=1

DEFINES += MINOR_VERSION=1

DEFINES += VERSION=\\\"4.1.1\\\"
