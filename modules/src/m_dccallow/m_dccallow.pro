TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_dccallow.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_dccallow/

TARGET = ../../../out/lib/clandestine/modules/m_dccallow

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
