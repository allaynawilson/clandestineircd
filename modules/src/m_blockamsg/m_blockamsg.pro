TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_blockamsg.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_blockamsg/

TARGET = ../../../out/lib/clandestine/modules/m_blockamsg

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
