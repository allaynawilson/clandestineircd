TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_ssl_mbedtls.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_ssl_mbedtls/

TARGET = ../../../out/lib/clandestine/modules/m_ssl_mbedtls

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
