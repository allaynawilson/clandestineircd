TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
m_joinflood.cpp

OBJECTS_DIR = ../../../out/obj/modules/m_joinflood/

TARGET = ../../../out/lib/clandestine/modules/m_joinflood

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
DEFINES += MODNAME=\\\"$$_PRO_FILE_\\\"
