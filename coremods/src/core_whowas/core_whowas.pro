TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_whowas.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_whowas/

TARGET = ../../../out/lib/clandestine/modules/core_whowas

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
