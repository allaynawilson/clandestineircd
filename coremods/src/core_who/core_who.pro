TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_who.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_who/

TARGET = ../../../out/lib/clandestine/modules/core_who

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
