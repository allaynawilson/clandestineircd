TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_serialize_rfc.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_serialize_rfc/

TARGET = ../../../out/lib/clandestine/modules/core_serialize_rfc

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
