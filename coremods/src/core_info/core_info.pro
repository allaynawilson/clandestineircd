TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
cmd_admin.cpp \
cmd_commands.cpp \
cmd_info.cpp \
cmd_modules.cpp \
cmd_motd.cpp \
cmd_servlist.cpp \
cmd_time.cpp \
cmd_version.cpp \
core_info.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_info/

TARGET = ../../../out/lib/clandestine/modules/core_info

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
