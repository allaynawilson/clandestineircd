TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_lusers.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_lusers/

TARGET = ../../../out/lib/clandestine/modules/core_lusers

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
