TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_whois.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_whois/

TARGET = ../../../out/lib/clandestine/modules/core_whois

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
