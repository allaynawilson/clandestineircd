TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_dns.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_dns/

TARGET = ../../../out/lib/clandestine/modules/core_dns

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
