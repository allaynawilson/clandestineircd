TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_stats.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_stats/

TARGET = ../../../out/lib/clandestine/modules/core_stats

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
