TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_stub.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_stub/

TARGET = ../../../out/lib/clandestine/modules/core_stub

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
