TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_hostname_lookup.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_hostname_lookup/

TARGET = ../../../out/lib/clandestine/modules/core_hostname_lookup

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
