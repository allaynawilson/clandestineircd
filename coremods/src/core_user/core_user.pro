TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
cmd_away.cpp \
cmd_ison.cpp \
cmd_nick.cpp \
cmd_part.cpp \
cmd_quit.cpp \
cmd_user.cpp \
cmd_userhost.cpp \
core_user.cpp \
umode_o.cpp \
umode_s.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_user/

TARGET = ../../../out/lib/clandestine/modules/core_user

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
