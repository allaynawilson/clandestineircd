TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_message.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_message/

TARGET = ../../../out/lib/clandestine/modules/core_message

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
