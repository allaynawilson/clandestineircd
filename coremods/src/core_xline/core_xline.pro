TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
cmd_eline.cpp \
cmd_gline.cpp \
cmd_kline.cpp \
cmd_qline.cpp \
cmd_zline.cpp \
core_xline.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_xline/

TARGET = ../../../out/lib/clandestine/modules/core_xline

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
