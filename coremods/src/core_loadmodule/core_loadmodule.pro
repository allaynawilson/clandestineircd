TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_loadmodule.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_loadmodule/

TARGET = ../../../out/lib/clandestine/modules/core_loadmodule

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
