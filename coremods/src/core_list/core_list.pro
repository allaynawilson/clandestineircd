TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_list.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_list/

TARGET = ../../../out/lib/clandestine/modules/core_list

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
