TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_reloadmodule.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_reloadmodule/

TARGET = ../../../out/lib/clandestine/modules/core_reloadmodule

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
