TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
cmd_die.cpp \
cmd_kill.cpp \
cmd_oper.cpp \
cmd_rehash.cpp \
cmd_restart.cpp \
core_oper.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_oper/

TARGET = ../../../out/lib/clandestine/modules/core_oper

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
