TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_mode.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_mode/

TARGET = ../../../out/lib/clandestine/modules/core_mode

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
