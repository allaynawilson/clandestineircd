TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
cmd_invite.cpp \
cmd_join.cpp \
cmd_kick.cpp \
cmd_names.cpp \
cmd_topic.cpp \
cmode_k.cpp \
cmode_l.cpp \
core_channel.cpp \
invite.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_channel/

TARGET = ../../../out/lib/clandestine/modules/core_channel

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
