TEMPLATE = lib

CONFIG = warn_off c++11 plugin

SOURCES += \
core_wallops.cpp

OBJECTS_DIR = ../../../out/obj/coremods/core_wallops/

TARGET = ../../../out/lib/clandestine/modules/core_wallops

INCLUDEPATH += ../../../

INCLUDEPATH += ../../include/
