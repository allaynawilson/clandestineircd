TEMPLATE = subdirs

CONFIG = warn_off c++11 shared

SUBDIRS = \
core_channel \
core_dns \
core_hostname_lookup \
core_info \
core_list \
core_loadmodule \
core_lusers \
core_message \
core_mode \
core_oper \
core_reloadmodule \
core_serialize_rfc \
core_stats \
core_stub \
core_user \
core_wallops \
core_who \
core_whois \
core_whowas \
core_xline

core_channel.subdir = src/core_channel
core_dns.subdir = src/core_dns
core_hostname_lookup.subdir = src/core_hostname_lookup
core_info.subdir = src/core_info
core_list.subdir = src/core_list
core_loadmodule.subdir = src/core_loadmodule
core_lusers.subdir = src/core_lusers
core_message.subdir = src/core_message
core_mode.subdir = src/core_mode
core_oper.subdir = src/core_oper
core_reloadmodule.subdir = src/core_reloadmodule
core_serialize_rfc.subdir = src/core_serialize_rfc
core_stats.subdir = src/core_stats
core_stub.subdir = src/core_stub
core_user.subdir = src/core_user
core_wallops.subdir = src/core_wallops
core_who.subdir = src/core_who
core_whois.subdir = src/core_whois
core_whowas.subdir = src/core_whowas
core_xline.subdir = src/core_xline
